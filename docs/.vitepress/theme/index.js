import DefaultTheme from 'vitepress/theme'
// import DarkTheme from 'vitepress-dark-theme/index.js'
import DemoContainer from '../components/DemoContainer.vue'
import DashVue from 'dash-vue'

import './custom.css'

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.use(DashVue)
    app.component('DemoContainer', DemoContainer)
  }
}
