const path = require('path')

module.exports = {
  title: 'My Lib',
  description: 'Just playing around.',
  outDir: '../public',
  themeConfig: {
    repo: 'https://github.com/djdietrick/dash-vue',
    sidebar: [
      {
        text: 'Introduction',
        children: [
          { text: 'What is My Lib?', link: '/' },
          { text: 'Getting Started', link: '/guide/' },
        ],
      }, {
        text: 'Components',
        children: [
          { text: 'Component A', link: '/components/component-a' },
          { text: 'Button', link: 'components/button'}
        ],
      }
    ],
  },
  vite: {
    resolve: {
      alias: {
        'dash-vue': path.resolve(__dirname, '../../src'),
      },
      dedupe: ['vue', /primevue\/.+/], // avoid error when using dependencies that also use Vue
    }
  }
}
