<script setup>
import Basic from './demo/Button.vue'
</script>

# Button

This is a simple button.

## Example Usage

<DemoContainer>
  <Basic/>
</DemoContainer>

<<< @/components/demo/Button.vue

## Reference

You may show props, slots, events, methods, etc. using Markdown.

### Properties

| Name | Type   | Default | Description    |
| ---- | ------ | ------- | -------------- |
| text | string | null    | Messge to show |
| primary | boolean | false | Use Primary color |

### Events

| Name | Parameters | Description |
| ---- | ---------- | ----------- |
|      |            |             |

### Slots

| Name | Parameters | Description |
| ---- | ---------- | ----------- |
|      |            |             |
