# What is My Lib?

This library is a starter to create your own Vue 3 component library in TypeScript.

- [ComponentA](/components/component-a) is a simple Hello World component with icon from bundled font file.
